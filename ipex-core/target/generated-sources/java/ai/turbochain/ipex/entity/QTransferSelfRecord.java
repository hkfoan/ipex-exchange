package ai.turbochain.ipex.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTransferSelfRecord is a Querydsl query type for TransferSelfRecord
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTransferSelfRecord extends EntityPathBase<TransferSelfRecord> {

    private static final long serialVersionUID = 1158734308L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QTransferSelfRecord transferSelfRecord = new QTransferSelfRecord("transferSelfRecord");

    public final NumberPath<java.math.BigDecimal> arrivedAmount = createNumber("arrivedAmount", java.math.BigDecimal.class);

    public final QCoin coin;

    public final DateTimePath<java.util.Date> createTime = createDateTime("createTime", java.util.Date.class);

    public final NumberPath<java.math.BigDecimal> fee = createNumber("fee", java.math.BigDecimal.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> legalcurrencyId = createNumber("legalcurrencyId", Long.class);

    public final NumberPath<Long> memberId = createNumber("memberId", Long.class);

    public final NumberPath<Integer> status = createNumber("status", Integer.class);

    public final NumberPath<java.math.BigDecimal> totalAmount = createNumber("totalAmount", java.math.BigDecimal.class);

    public final NumberPath<Integer> type = createNumber("type", Integer.class);

    public final NumberPath<Long> walletId = createNumber("walletId", Long.class);

    public QTransferSelfRecord(String variable) {
        this(TransferSelfRecord.class, forVariable(variable), INITS);
    }

    public QTransferSelfRecord(Path<? extends TransferSelfRecord> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QTransferSelfRecord(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QTransferSelfRecord(PathMetadata metadata, PathInits inits) {
        this(TransferSelfRecord.class, metadata, inits);
    }

    public QTransferSelfRecord(Class<? extends TransferSelfRecord> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.coin = inits.isInitialized("coin") ? new QCoin(forProperty("coin")) : null;
    }

}

