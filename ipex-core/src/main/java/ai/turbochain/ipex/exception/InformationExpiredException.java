package ai.turbochain.ipex.exception;

/**
 * @author jack
 * @date 2018年01月18日
 */
public class InformationExpiredException extends Exception {
    public InformationExpiredException(String msg) {
        super(msg);
    }
}
